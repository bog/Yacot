/* Menu */
#ifndef MENU_HPP
#define MENU_HPP
#include <iostream>
#include <Scene.hpp>

enum MenuState
{
  MENU_PLAY=0,
  MENU_ABOUT=1,
  MENU_QUIT=2
};

class Menu: public Scene
{
public:
  Menu(Core* owner);
  virtual ~Menu();

  void update() override;
  void display(sf::RenderWindow& window) override;
  
protected:
  
private:
  Menu( Menu const& menu ) = delete;
  Menu& operator=( Menu const& menu ) = delete;

  sf::Font m_font;
  sf::Text m_title;
  
  sf::Text m_subtitle;
  sf::RectangleShape m_subtitle_rect;
  
  int m_menu_state;

  sf::Text m_play;
  sf::Text m_about;
  sf::Text m_quit;

  sf::Color m_selected_color;
  sf::Color m_unselected_color;

  sf::Clock m_navigate_clock;
  float m_navigate_delay;
  
};

#endif
