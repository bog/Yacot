/* Core */
#ifndef CORE_HPP
#define CORE_HPP
#include <iostream>
#include <SFML/Graphics.hpp>
#include <Define.hpp>

class Scene;
class ResourceLoader;

class Core
{
public:
  Core();
  virtual ~Core();

  void update();
  void display(sf::RenderWindow &window);
  void render(sf::RenderWindow &window);

  ResourceLoader * getResources();
  
  /* SCENE */
  void newGame();
  void gameOver(int score);
  void menu();
  void about();
  
  void quit();
protected:
  
private:
  Core( Core const& core ) = delete;
  Core& operator=( Core const& core ) = delete;
  void setCurrentScene(Scene* scene);

  ResourceLoader *m_resources;
  Scene *m_current_scene;
  bool m_running;
};

#endif
