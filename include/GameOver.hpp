/* GameOver */
#ifndef GAMEOVER_HPP
#define GAMEOVER_HPP
#include <iostream>
#include <Scene.hpp>

enum GAME_OVER_STATE 
  {
    GAME_OVER_NEW,
    GAME_OVER_MENU,
    GAME_OVER_QUIT
  };

class GameOver: public Scene
{
public:
  GameOver(Core* owner, int score);
  virtual ~GameOver();

  void update() override;
  void display(sf::RenderWindow& window) override;
  
protected:
  
private:
  GameOver( GameOver const& gameover ) = delete;
  GameOver& operator=( GameOver const& gameover ) = delete;

  sf::Font m_font;
  sf::Text m_go_text;

  sf::Text m_your_score;
  sf::RectangleShape m_score_rect;
  
  int m_menu_state;
  sf::Text m_new;
  sf::Text m_menu;
  sf::Text m_quit;


  sf::Color m_selected_color;
  sf::Color m_unselected_color;

  sf::Clock m_navigate_clock;
  float m_navigate_delay;

  int m_score;
  
};

#endif
