/* Scene */
#ifndef SCENE_HPP
#define SCENE_HPP
#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <Define.hpp>

class Core;

class Scene
{
public:
  Scene(Core *owner);
  virtual ~Scene();

  virtual void update();
  virtual void display(sf::RenderWindow &window);
  
protected:
  Core *m_owner;
private:
  Scene( Scene const& scene ) = delete;
  Scene& operator=( Scene const& scene ) = delete;


  
};

#endif
