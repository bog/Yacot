/* InGame */
#ifndef INGAME_HPP
#define INGAME_HPP

#include <iostream>
#include <Scene.hpp>
#include <chrono>

class Group;
class Block;
enum class BlockType;
class Rectangle;

class InGame: public Scene
{
public:
  InGame(Core *owner);
  virtual ~InGame();

  virtual void update();

  /* BLOCKS */
  void updateBlock();
  void updateKeyboard();
  void updateLines();

  bool collideWithBoard(int I, int J);
  virtual void display(sf::RenderWindow &window);

  void anotherBlock();

  bool isLineFull(size_t l);
  void flushLine(size_t l);
    
  int randNumber(int a, int b);
  BlockType randBlockType();

  void printBoard();

  /* INTERFACE */
  void defineNextBlock();
  
protected:
  
private:
  InGame( InGame const& InGame ) = delete;
  InGame& operator=( InGame const& InGame ) = delete;
  
  Group *m_interface;
  
  sf::Clock m_rotation_clock;
  float m_rotation_delay;

  sf::Clock m_fall_clock;
  float m_fall_delay;
  float m_fall_delay_base;

  sf::Clock m_move_clock;
  float m_move_delay;
  
  Block *m_current_block;

  bool m_game_over;
  
  Rectangle* m_board[NB_BLOCK_HEIGHT][NB_BLOCK_WIDTH];



  BlockType m_type_next_block;
  Block *m_next_block;

  /* SCORE */
  int m_score = 0;
  sf::Font m_score_font;
  sf::Text m_score_text;
};

#endif
