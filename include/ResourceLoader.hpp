/* ResourceLoader */
#ifndef RESOURCELOADER_HPP
#define RESOURCELOADER_HPP
#include <iostream>

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include <unordered_map>


class ResourceLoader
{
public:
  ResourceLoader();
  virtual ~ResourceLoader();

  sf::Font getFont(std::string name);
  void playSound(std::string name);
  void waitSound(std::string name);
  void playMusic(std::string name);
  void stopMusic(std::string name);
protected:
  
private:
  ResourceLoader( ResourceLoader const& resourceloader ) = delete;
  ResourceLoader& operator=( ResourceLoader const& resourceloader ) = delete;

  void load();
  void loadFont(std::string name, std::string path);
  void loadSound(std::string name, std::string path);
  void loadMusic(std::string name, std::string path);
  
  std::unordered_map<std::string, sf::Font*> m_fonts;
  std::unordered_map<std::string, sf::Music* > m_musics;
  std::unordered_map<std::string, sf::Sound* > m_sounds;
  std::unordered_map<std::string, sf::SoundBuffer* > m_sounds_buffers;
  
};

#endif
