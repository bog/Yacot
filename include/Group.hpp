/* Group */
#ifndef GROUP_HPP
#define GROUP_HPP
#include <iostream>
#include <vector>
#include <SFML/Graphics.hpp>

class Entity;

class Group
{
public:
  Group();
  virtual ~Group();

  virtual void update();
  virtual void display(sf::RenderWindow &window);

  virtual void add(Entity* entity);
  
  void remove(Entity *entity);
  void remove(size_t i);
  
  Entity* get(size_t index) const;
    
protected:
  
private:
  Group( Group const& group ) = delete;
  Group& operator=( Group const& group ) = delete;

  std::vector<Entity*> m_entities;
  
};

#endif
