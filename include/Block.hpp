/* Block */
#ifndef BLOCK_HPP
#define BLOCK_HPP

#include <iostream>
#include <vector>
#include <SFML/Graphics.hpp>
#include <Entity.hpp>

class Rectangle;



 

enum class BlockType
  {
    I,
    J,
    L,
    O,
    S,
    T,
    Z
  };

struct BlockPart
{
  Rectangle* r;
  ssize_t i;
  ssize_t j;
};

class Block : public Entity
{
public:

  static sf::Color COLOR_I;
  static sf::Color COLOR_J;
  static sf::Color COLOR_L;
  static sf::Color COLOR_O;
  static sf::Color COLOR_S;
  static sf::Color COLOR_T;
  static sf::Color COLOR_Z;

  
  Block(BlockType type);
  Block(int x, int y, BlockType type);
  
  virtual ~Block();
  
  virtual void update() override;
  virtual void display(sf::RenderWindow &window) override;

  virtual const int zindex() const {return 2;}

  bool isActive() const {return m_active;}
  void desactive(){m_active = false;}
  
  void move(int x,int y);
  void rotate();
  std::vector<Rectangle*> explode();
  
  BlockPart* get(ssize_t i){return m_blocks[i];}
  
  sf::Vector2f getPosition() const{return m_position;}

  size_t getXMax();
  size_t getXMin();
  sf::Vector2f getHeadPosition();

  void reshape(BlockType type);
  
protected:
  
private:
  Block( Block const& block ) = delete;
  Block& operator=( Block const& block ) = delete;
  BlockType m_type;
  std::vector<BlockPart*> m_blocks;

  sf::Vector2f m_position;
  
  void init();
  size_t computeHead();
  void create_block(ssize_t i, ssize_t j, sf::Color color);

  bool m_active;
  size_t m_head;

};

#endif
