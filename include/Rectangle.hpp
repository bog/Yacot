/* Rectangle */
#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP
#include <iostream>
#include <Entity.hpp>
#include <SFML/Graphics.hpp>
#include <Define.hpp>

class Rectangle: public Entity
{
public:
  Rectangle(int x,int y, int width, int height, sf::Color color, int index);
  Rectangle(int width, int height, sf::Color color, int index);
  
  virtual ~Rectangle();

  virtual void update();
  virtual void display(sf::RenderWindow &window);

  virtual const int zindex() const {return m_zindex;}
  
  void move(int x,int y);
  sf::Vector2f getPosition() const;
  void setPosition(sf::Vector2f pos);
  void setColor(sf::Color color);

  sf::Color getColor();
  
protected:
  
private:
  Rectangle( Rectangle const& rectangle ) = delete;
  Rectangle& operator=( Rectangle const& rectangle ) = delete;

  sf::RectangleShape m_rs;
  int m_zindex;
};

#endif
