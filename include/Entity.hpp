/* Entity */
#ifndef ENTITY_HPP
#define ENTITY_HPP
#include <iostream>
#include <SFML/Graphics.hpp>

class Entity
{
public:
  Entity();
  virtual ~Entity();

  virtual void update();
  virtual void display(sf::RenderWindow &window);

  virtual const int zindex() const = 0;
  
protected:
  
private:
  Entity( Entity const& entity ) = delete;
  Entity& operator=( Entity const& entity ) = delete;
  
  
};

#endif
