#ifndef DEFINE_HPP
#define DEFINE_HPP


#define SIDE_WIDTH 300

#define BLOCK_WIDTH 32
#define BLOCK_HEIGHT 32
#define NB_BLOCK_WIDTH 15
#define NB_BLOCK_HEIGHT 20

#define SIDE_X ((NB_BLOCK_WIDTH+1)*BLOCK_WIDTH)

#define SCREEN_WIDTH ( (BLOCK_WIDTH * NB_BLOCK_WIDTH) + SIDE_WIDTH )
#define SCREEN_HEIGHT (BLOCK_HEIGHT * NB_BLOCK_HEIGHT)
#define FRAMERATE 10000


#define SCORE_LINE_FULL 200








#endif
