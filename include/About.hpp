/* About */
#ifndef ABOUT_HPP
#define ABOUT_HPP
#include <iostream>
#include <Scene.hpp>

enum AboutState
{
  ABOUT_BACK=0,
  ABOUT_QUIT=1
};

class About: public Scene
{
public:
  About(Core* owner);
  virtual ~About();

  void update() override;
  void display(sf::RenderWindow& window) override;
  
protected:
  
private:
  About( About const& about ) = delete;
  About& operator=( About const& about ) = delete;

  sf::Font m_font;
  sf::Text m_title;
  
  sf::Text m_author;
  sf::RectangleShape m_author_rect;
  
  int m_about_state;

  sf::Text m_back;
  sf::Text m_quit;

  sf::Text m_describe;

  sf::Color m_selected_color;
  sf::Color m_unselected_color;

  sf::Clock m_navigate_clock;
  float m_navigate_delay;
  
};

#endif
