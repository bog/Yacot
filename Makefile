CXX= clang++
CXXFLAGS= -Wall -std=c++11 -g -I"include/" 
EXEC= yacot.out
SRC=$(wildcard src/*.cpp)
OBJ=$(SRC:.cpp=.o)


$(EXEC): $(OBJ)
	$(CXX) $(CXXFLAGS) $^ -lsfml-audio -lsfml-graphics -lsfml-window -lsfml-system -o $@

clean:
	rm -rf src/*.o

mrproper: clean
	rm -rf $(EXEC)
