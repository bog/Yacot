#include <Core.hpp>
#include <Scene.hpp>
#include <chrono>
#include <Menu.hpp>
#include <InGame.hpp>
#include <GameOver.hpp>
#include <ResourceLoader.hpp>
#include <About.hpp>

#include <cassert>

Core::Core() : m_running( true)
{
  m_resources = new ResourceLoader();
  
  m_current_scene = new Menu(this);

}

ResourceLoader* Core::getResources()
{
  assert( m_resources != nullptr );
  
  return m_resources;
}

void Core::quit()
{
  m_running = false;

}

void Core::update()
{
  if( m_current_scene != nullptr )
    {
      m_current_scene->update();
    }
}

void Core::display(sf::RenderWindow &window)
{
  window.clear(sf::Color(4,139,154));

  if( m_current_scene != nullptr )
    {
      m_current_scene->display(window);
    }
}

void Core::setCurrentScene(Scene* scene)
{
  if( m_current_scene != nullptr )
    {
      delete scene;
    }

  m_current_scene = scene;
}

void Core::newGame()
{
  setCurrentScene(nullptr);
  setCurrentScene(new InGame(this));
}

void Core::menu()
{
  setCurrentScene(nullptr);
  setCurrentScene(new Menu(this));
}

void Core::about()
{
  setCurrentScene(nullptr);
  setCurrentScene(new About(this));
}

void Core::gameOver(int score)
{
  setCurrentScene(nullptr);
  setCurrentScene(new GameOver(this, score));
}

void Core::render(sf::RenderWindow &window)
{
  sf::Event event;
  //const int US_PER_FRAME = 1000000/FRAMERATE;
  std::chrono::steady_clock::time_point previous = std::chrono::steady_clock::now();
  int lag = 0;
  
  while( window.isOpen() && m_running )
    {
      /* TIME */
      std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
      int elapsed = std::chrono::duration_cast<std::chrono::microseconds>(now - previous).count();
      previous = now;

      lag += elapsed;

      
      /* EVENTS */
      while( window.pollEvent(event) )
	{
	  switch( event.type )
	    {
	    case sf::Event::Closed:
	      window.close();
	      break;

	    case sf::Event::KeyPressed:
	      
	      switch( event.key.code )
		{
		case sf::Keyboard::Escape:
		  window.close();
		  break;
		  
		default: break;
		}
	      
	      break;
	      
	    default: break;
	    }
	}

      /* UPDATE */
      /*
      while( lag >= US_PER_FRAME )
	{
	  update();
	  lag -= US_PER_FRAME;
	}
      */
      update();
      /* DISPLAY */
      display(window);
      window.display();
    }
}


Core::~Core()
{
  if( m_current_scene != nullptr )
    {
      delete m_current_scene;
    }

  delete m_resources;
}
