#include <iostream>
#include <SFML/Graphics.hpp>
#include <Core.hpp>
#include <Define.hpp>

int main(int argc, char** argv)
{
  sf::RenderWindow window(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "Yacot | Yet Another Clone Of Tetris");
  window.setFramerateLimit(FRAMERATE);
  
  Core core;
  core.render(window);
  
  return 0;
}
