#include <Block.hpp>
#include <Rectangle.hpp>

sf::Color Block::COLOR_I = sf::Color(150, 100, 100);
sf::Color Block::COLOR_J = sf::Color(100, 100, 100);
sf::Color Block::COLOR_L = sf::Color(150, 100, 150);
sf::Color Block::COLOR_O = sf::Color(100, 100, 150);
sf::Color Block::COLOR_S = sf::Color(100, 150, 100);
sf::Color Block::COLOR_T = sf::Color(150, 150, 100);
sf::Color Block::COLOR_Z = sf::Color(100, 150, 150);

Block::Block(int x, int y, BlockType type): m_type(type), m_position(sf::Vector2f(x, y)), m_active(true)
{
  init();
  m_head = computeHead();
}

Block::Block(BlockType type): Block(0,0, type)
{
}

void Block::reshape(BlockType type)
{
    for( BlockPart* bp : m_blocks )
    {
      delete bp->r;
      delete bp;
    }
    
    m_blocks.erase(m_blocks.begin(), m_blocks.end());
    
    m_type = type;
    init();
    m_head = computeHead();
    update();
}

std::vector<Rectangle*> Block::explode()
{
  std::vector<Rectangle*> vec;
  
  for(size_t i=0; i< m_blocks.size(); i++)
    {
      vec.push_back(m_blocks[i]->r);
    }

  return vec;
}

void Block::create_block(ssize_t i, ssize_t j, sf::Color color)
{
  BlockPart *bp = new BlockPart;
  
  bp->r = new Rectangle(m_position.x + BLOCK_HEIGHT * j, m_position.y + BLOCK_WIDTH * i, BLOCK_WIDTH, BLOCK_HEIGHT, color, zindex());

  bp->i = i;
  bp->j = j;
  
  m_blocks.push_back(bp);
}

size_t Block::getXMax()
{
  size_t max = 0;
    
  for(size_t i=0; i<m_blocks.size(); i++)
    {
      if( m_blocks[i]->j >= m_blocks[max]->j )
	{
	  max = i;
	}
    }

  return max;
}

size_t Block::getXMin()
{
  size_t min = 0;
    
  for(size_t i=0; i<m_blocks.size(); i++)
    {
      if( m_blocks[i]->j < m_blocks[min]->j )
	{
	  min = i;
	}
    }

  return min;
}

sf::Vector2f Block::getHeadPosition()
{
  return sf::Vector2f(m_blocks[m_head]->r->getPosition().x, m_blocks[m_head]->r->getPosition().y);
}

void Block::init()
{
  switch(m_type)
    {
    case BlockType::I:
      create_block(0,-1, COLOR_I);
      create_block(0,0, COLOR_I);
      create_block(0,1, COLOR_I);
      create_block(0,2, COLOR_I);
      break;

    case BlockType::J:
      create_block(0,-1, COLOR_J);
      create_block(0,0, COLOR_J);
      create_block(0,1, COLOR_J);
      create_block(1,1, COLOR_J);
      break;

    case BlockType::L:
      create_block(0,-1, COLOR_L);
      create_block(0,0, COLOR_L);
      create_block(0,1, COLOR_L);
      create_block(1,-1, COLOR_L);
      break;

    case BlockType::O:
      create_block(0,0, COLOR_O);
      create_block(0,1, COLOR_O);
      create_block(1,1, COLOR_O);
      create_block(1,0, COLOR_O);
      break;

    case BlockType::S:
      create_block(-1,0, COLOR_S);
      create_block(-1,1, COLOR_S);
      create_block(0,-1, COLOR_S);
      create_block(0,0, COLOR_S);
      break;

    case BlockType::T:
      create_block(0,-1, COLOR_T);
      create_block(0,0, COLOR_T);
      create_block(0,1, COLOR_T);
      create_block(1,0, COLOR_T);
      break;

    case BlockType::Z:
      create_block(-1,-1, COLOR_Z);
      create_block(-1,0, COLOR_Z);
      create_block(0,0, COLOR_Z);
      create_block(0,1, COLOR_Z);
      break;
    }
}

void Block::rotate()
{
  if( m_type == BlockType::O ){ return; }

  for( BlockPart* bp : m_blocks )
    {
      float j = bp->j*cos(M_PI_2) - bp->i*sin(M_PI_2);
      float i = bp->j*sin(M_PI_2) + bp->i*cos(M_PI_2) ;

      bp->j = j;
      bp->i = i;
    }

  m_head= computeHead();

  update();
  
}

size_t Block::computeHead()
{
  size_t head = 0;

  for(size_t i=0; i<m_blocks.size(); i++)
    {
      if( m_blocks[i]->i >= m_blocks[head]->i )
	{
	  head = i;
	}
    }

  return head;
}

void Block::update()
{

  for( BlockPart* bp : m_blocks )
    {
      bp->r->setPosition( sf::Vector2f(m_position.x + bp->j*BLOCK_WIDTH, m_position.y + bp->i*BLOCK_HEIGHT) );
      bp->r->update();
    }

  /* Still active ? */
  m_active = !(m_blocks[m_head]->r->getPosition().y/BLOCK_HEIGHT >= NB_BLOCK_HEIGHT - 1 );


}

void Block::display(sf::RenderWindow &window)
{
  for( BlockPart* bp : m_blocks )
    {
      bp->r->display(window);
    }

}

void Block::move(int x,int y)
{
  m_position.x += x;
  m_position.y += y;
}

Block::~Block()
{
  for( BlockPart* bp : m_blocks )
    {
      delete bp->r;
      delete bp;
    }

  m_blocks.erase(m_blocks.begin(), m_blocks.end());
  
}
