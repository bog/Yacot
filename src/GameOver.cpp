#include <GameOver.hpp>
#include <ResourceLoader.hpp>
#include <Core.hpp>

GameOver::GameOver(Core *owner, int score): Scene(owner), m_menu_state(GAME_OVER_NEW),
				 m_selected_color(sf::Color(200,50,200)),
				 m_unselected_color(sf::Color(50,50,50)),
					    m_navigate_delay(200), m_score(score)
{
  
  /* GAME OVER */
  m_font = m_owner->getResources()->getFont("madpixels");
  m_go_text.setFont(m_font);
  m_go_text.setColor(sf::Color::Red);
  m_go_text.setCharacterSize(100);
  m_go_text.setString("Game Over !");

  float x = (SCREEN_WIDTH - m_go_text.getLocalBounds().width)/2;
  float y = 3*SCREEN_HEIGHT/12 ;
  m_go_text.setPosition(sf::Vector2f(x, y));

  /* YOUR SCORE */
  m_your_score.setFont(m_font);
  m_your_score.setColor(sf::Color(200, 200, 230));
  m_your_score.setString("Your score : " + std::to_string(m_score) + " pts ");

  y += m_go_text.getLocalBounds().height + m_your_score.getLocalBounds().height;
  m_your_score.setPosition(sf::Vector2f(x, y));

  /* YOUR SCORE RECT*/
  m_score_rect.setPosition(sf::Vector2f(0, y));
  m_score_rect.setSize(sf::Vector2f(SCREEN_WIDTH, 1.2* m_your_score.getLocalBounds().height));
  m_score_rect.setFillColor(sf::Color::Red);
  
  /* NEW */
  m_new.setFont(m_font);
  m_new.setString("New game");
  m_new.setColor(sf::Color::Black);
  
  x = (SCREEN_WIDTH - m_new.getLocalBounds().width)/2;
  y = 7*SCREEN_HEIGHT/12;
  m_new.setPosition(sf::Vector2f(x, y));

  /* MENU */
  m_menu.setFont(m_font);
  m_menu.setString("Back to menu");
  m_menu.setColor(sf::Color::Black);
  
  x = (SCREEN_WIDTH - m_menu.getLocalBounds().width)/2;
  y = 8*SCREEN_HEIGHT/12;
  m_menu.setPosition(sf::Vector2f(x, y));
  
  /* QUIT */
  m_quit.setFont(m_font);
  m_quit.setString("Quit");
  m_quit.setColor(sf::Color::Black);
  
  x = (SCREEN_WIDTH - m_quit.getLocalBounds().width)/2;
  y = 9*SCREEN_HEIGHT/12;
  m_quit.setPosition(sf::Vector2f(x, y));

  m_owner->getResources()->playMusic("gameover");
  m_navigate_clock.restart();
}


void GameOver::update()
{
  /* KEYBOARD */
  //up
  if( sf::Keyboard::isKeyPressed(sf::Keyboard::Up)
      && m_navigate_clock.getElapsedTime().asMilliseconds() >= m_navigate_delay )
    {
      m_menu_state--;
      if( m_menu_state < GAME_OVER_NEW )
	{
	  m_menu_state = GAME_OVER_QUIT;
	}

      m_owner->getResources()->playSound("navigate");
      
      m_navigate_clock.restart();
    }

  //down
  if( sf::Keyboard::isKeyPressed(sf::Keyboard::Down)
      && m_navigate_clock.getElapsedTime().asMilliseconds() >= m_navigate_delay )
    {
      m_menu_state++;
      if( m_menu_state > GAME_OVER_QUIT )
	{
	  m_menu_state = GAME_OVER_NEW;
	}

      m_owner->getResources()->playSound("navigate");
      
      m_navigate_clock.restart();
    }

  //ok
  if( sf::Keyboard::isKeyPressed(sf::Keyboard::Return)
      || sf::Keyboard::isKeyPressed(sf::Keyboard::Space) )
    {
      m_owner->getResources()->playSound("select");
      m_owner->getResources()->stopMusic("gameover");
      sf::Clock chrono;

      while(chrono.getElapsedTime().asMilliseconds() < 1000 ){}
      
      switch( m_menu_state )
	{
	case GAME_OVER_NEW:
	  m_owner->newGame();
	  break;
	  
	case GAME_OVER_MENU:
	  m_owner->menu();
	  break;
	  
	case GAME_OVER_QUIT:
	  m_owner->quit();
	  break;
	}
    }

  /* COLORS */
  switch( m_menu_state )
    {
    case GAME_OVER_NEW:
      m_new.setColor(m_selected_color);
      m_menu.setColor(m_unselected_color);
      m_quit.setColor(m_unselected_color);
      break;
      
    case GAME_OVER_MENU:
      m_new.setColor(m_unselected_color);
      m_menu.setColor(m_selected_color);
      m_quit.setColor(m_unselected_color);
      break;
      
    case GAME_OVER_QUIT:
      m_new.setColor(m_unselected_color);
      m_menu.setColor(m_unselected_color);
      m_quit.setColor(m_selected_color);
      break;
    }
}

void GameOver::display(sf::RenderWindow& window)
{
  //  window.clear(sf::Color(100, 200 ,100));
  window.clear(sf::Color(200, 200 , 230));
  window.draw(m_go_text);
  window.draw(m_score_rect);
  window.draw(m_your_score);
  window.draw(m_new);
  window.draw(m_menu);
  window.draw(m_quit);
}

GameOver::~GameOver()
{
  
}
