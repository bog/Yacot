#include <About.hpp>

#include <Core.hpp>
#include <ResourceLoader.hpp>

About::About(Core *owner): Scene(owner), m_about_state(ABOUT_BACK),
				 m_selected_color(sf::Color(200,50,200)),
				 m_unselected_color(sf::Color(50,50,50)),
				 m_navigate_delay(200)
{


  
  /* TITLE */
  m_font = m_owner->getResources()->getFont("madpixels");
    
  m_title.setFont(m_font);
  m_title.setCharacterSize(100);
  m_title.setColor(sf::Color(150, 150, 20));
  m_title.setString("ABOUT");

  float x = (SCREEN_WIDTH - m_title.getLocalBounds().width)/2;
  float y = 2*SCREEN_HEIGHT/12 ;
  m_title.setPosition(sf::Vector2f(x, y));

  /* AUTHOR */
  m_author.setFont(m_font);
  m_author.setCharacterSize(20);
  m_author.setColor(sf::Color(200, 230, 200));
  m_author.setString("By Bog - April 2015");

  x = (SCREEN_WIDTH - m_author.getLocalBounds().width)/2;
  y += m_title.getLocalBounds().height + m_author.getLocalBounds().height;
  m_author.setPosition(sf::Vector2f(x, y));


  /* SUBTITILE RECT */
  m_author_rect.setSize(sf::Vector2f(SCREEN_WIDTH, 1.2 * m_author.getLocalBounds().height));
  m_author_rect.setPosition(sf::Vector2f(0, y));
  m_author_rect.setFillColor(sf::Color(150, 150, 20));

  /* DESCRIBE */
  m_describe.setFont(m_font);
  m_describe.setCharacterSize(20);
  
  std::string str_describe = "YACOT - Yet Another Clone Of Tetris - is a simple clone of tetris \nmade in two weeks.\n\nYACOT is released under the term of the \nGNU General Public Licence.\n\n\nEnjoy ! :)";

  m_describe.setColor(sf::Color(20, 20, 20));
  m_describe.setString(str_describe);

  x = (SCREEN_WIDTH - m_describe.getLocalBounds().width)/2;
  y = 5*SCREEN_HEIGHT/12;
  m_describe.setPosition(sf::Vector2f(x, y));
  
  /* BACK */
  m_back.setFont(m_font);
  m_back.setString("Back");
  m_back.setColor(sf::Color::Black);
  
  x = (SCREEN_WIDTH - m_back.getLocalBounds().width)/2;
  y = 9*SCREEN_HEIGHT/12;
  m_back.setPosition(sf::Vector2f(x, y));
  
  /* QUIT */
  m_quit.setFont(m_font);
  m_quit.setString("Quit");
  m_quit.setColor(sf::Color::Black);
  
  x = (SCREEN_WIDTH - m_quit.getLocalBounds().width)/2;
  y = 10*SCREEN_HEIGHT/12;
  m_quit.setPosition(sf::Vector2f(x, y));


  m_navigate_clock.restart();

  m_owner->getResources()->playMusic("menu");
}


void About::update()
{
  /* KEYBOARD */
  //up
  if(  sf::Keyboard::isKeyPressed(sf::Keyboard::Up)
      && m_navigate_clock.getElapsedTime().asMilliseconds() >= m_navigate_delay )
    {
      m_about_state--;
      
      if( m_about_state < ABOUT_BACK )
	{
	  m_about_state = ABOUT_QUIT;
	}
      
      m_owner->getResources()->playSound("navigate");
      m_navigate_clock.restart();
    }
       
  //down
  if(  sf::Keyboard::isKeyPressed(sf::Keyboard::Down)
      && m_navigate_clock.getElapsedTime().asMilliseconds() >= m_navigate_delay )
    {
      m_about_state++;
      
      if( m_about_state > ABOUT_QUIT )
	{
	  m_about_state = ABOUT_BACK;
	}
      
      m_owner->getResources()->playSound("navigate");
      m_navigate_clock.restart();
    }

  //ok
  if( sf::Keyboard::isKeyPressed(sf::Keyboard::Return)
      || sf::Keyboard::isKeyPressed(sf::Keyboard::Space) )
    {
      sf::Clock chrono;
      
      m_owner->getResources()->playSound("select");
      m_owner->getResources()->stopMusic("menu");
      
      //waiting for the sound
      while( chrono.getElapsedTime().asMilliseconds() < 1000 )
	{
	}
      
      switch( m_about_state )
	{
	case ABOUT_BACK:
	  m_owner->menu();
	  break;

	case ABOUT_QUIT:

	  m_owner->quit();
	  break;
	}
    }

  /* COLORS */
  switch( m_about_state )
    {
    case ABOUT_BACK:
      m_back.setColor(m_selected_color);
      m_quit.setColor(m_unselected_color);
      break;

    case ABOUT_QUIT:
      m_back.setColor(m_unselected_color);
      m_quit.setColor(m_selected_color);
      break;
    }
}

void About::display(sf::RenderWindow& window)
{
  window.clear(sf::Color(200, 200 ,230));
  window.draw(m_title);
  window.draw(m_author_rect);
  window.draw(m_author);
  window.draw(m_describe);
  window.draw(m_back);
  window.draw(m_quit);
}

About::~About()
{
  
}
