#include <ResourceLoader.hpp>
#include <cassert>

ResourceLoader::ResourceLoader()
{
  load();
}


void ResourceLoader::load()
{
  /* FONTS */
  loadFont("madpixels","data/font/madpixels.otf");
  loadFont("alex","data/font/alex-toth.ttf");

  /* SOUNDS */
  loadSound("navigate","data/sound/navigate.ogg");
  loadSound("select","data/sound/select.ogg");
  loadSound("fall","data/sound/fall.ogg");
  loadSound("ground","data/sound/ground.ogg");
  loadSound("line","data/sound/line.ogg");
  loadSound("rotate","data/sound/rotate.ogg");
  loadSound("end","data/sound/end.ogg");

  /* MUSICS */
  loadMusic("menu","data/music/menu.ogg");
  loadMusic("gameover","data/music/gameover.ogg");
  loadMusic("game","data/music/game.ogg");
}

void ResourceLoader::loadFont(std::string name, std::string path)
{
  sf::Font* font = new sf::Font;
  font->loadFromFile(path);

  m_fonts[name] = font;
}

void ResourceLoader::loadMusic(std::string name, std::string path)
{
  sf::Music* music = new sf::Music;
  music->openFromFile(path);
  music->setLoop(true);
  m_musics[name] = music;
}

void ResourceLoader::loadSound(std::string name, std::string path)
{
  sf::Sound *s = new sf::Sound;
  
  sf::SoundBuffer *sb = new sf::SoundBuffer;
  sb->loadFromFile(path);
  s->setBuffer(*sb);
  s->setVolume(10);
  m_sounds[name] = s;
  m_sounds_buffers[name] = sb;
}


sf::Font ResourceLoader::getFont(std::string name)
{
  sf::Font *font = m_fonts[name];
  assert( font != nullptr );
  
  return *font;
}

void ResourceLoader::playSound(std::string name)
{
  sf::Sound* sound = m_sounds[name];
  assert(sound != nullptr);
  
  sound->play();
}

void ResourceLoader::waitSound(std::string name)
{
  sf::Sound* sound = m_sounds[name];
  assert(sound != nullptr);
  
  while( sound->getStatus() == sf::Sound::Playing ){}
}

void ResourceLoader::playMusic(std::string name)
{
  sf::Music* music = m_musics[name];
  assert(music != nullptr);
  
  music->play();
}

void ResourceLoader::stopMusic(std::string name)
{
  sf::Music* music = m_musics[name];
  assert(music != nullptr);
  
  music->stop();
}

ResourceLoader::~ResourceLoader()
{
  for( auto it = m_fonts.begin(); it != m_fonts.end(); it++ )
    {
      delete it->second;
    }

  for( auto it = m_sounds.begin(); it != m_sounds.end(); it++ )
    {
      delete it->second;
    }

  for( auto it = m_sounds_buffers.begin(); it != m_sounds_buffers.end(); it++ )
    {
      delete it->second;
    }

    for( auto it = m_musics.begin(); it != m_musics.end(); it++ )
    {
      delete it->second;
    }
}
