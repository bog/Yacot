#include <Rectangle.hpp>

Rectangle::Rectangle(int x,int y, int width, int height, sf::Color color, int index) : m_zindex(index)
{
  m_rs = sf::RectangleShape(sf::Vector2f(width, height));
  m_rs.setPosition(sf::Vector2f(x,y));
  m_rs.setFillColor(color);

}

Rectangle::Rectangle(int width, int height, sf::Color color, int index): Rectangle(0, 0, width, height, color, index)
{
}

void Rectangle::move(int x,int y)
{
  m_rs.move(x,y);
}

sf::Vector2f Rectangle::getPosition() const
{
  return m_rs.getPosition();
}

void Rectangle::setPosition(sf::Vector2f pos) 
{
  m_rs.setPosition(pos);
}

void Rectangle::setColor(sf::Color color)
{
  m_rs.setFillColor(color);
}

sf::Color Rectangle::getColor()
{
  return m_rs.getFillColor();
}

void Rectangle::update()
{
  
}

void Rectangle::display(sf::RenderWindow &window)
{
  window.draw(m_rs);
}

Rectangle::~Rectangle()
{
  
}
