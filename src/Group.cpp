#include <Group.hpp>
#include <Entity.hpp>
#include <cassert>

Group::Group()
{

}

void Group::update()
{
  /* update entities */
  for(Entity* e: m_entities)
    {
      if( e != nullptr )
	{
	  e->update();
	}
    }
}

void Group::display(sf::RenderWindow &window)
{
  for(Entity* e: m_entities)
    {
      if( e != nullptr )
	{
	  e->display(window);
	}
    }
}


void Group::add(Entity* entity)
{
  assert( entity != nullptr );
  m_entities.push_back(entity);

  /* sort entities */
  //  std::sort(m_entities.begin(), m_entities.end(), [](Entity const* e1, Entity const* e2){return e1->zindex() < e2->zindex();} );
  
}

void Group::remove(size_t i)
{
  assert( m_entities[i] != nullptr );

  delete m_entities[i];
  m_entities[i] = nullptr;

  //m_entities.erase(m_entities.begin() + i);
  
}

void Group::remove(Entity *entity)
{
  int i = 0;
  for(Entity *e : m_entities)
    {
      if( e == entity )
	{
	  remove(i);
	  return;
	}

      i++;
    }
}

Entity* Group::get(size_t index) const
{
  assert(index>=0);
  assert(index<m_entities.size());

  return m_entities[index];
}

Group::~Group()
{
  
  size_t size = m_entities.size();
  
  for(size_t i=0; i<size; i++)
    {
      if( m_entities[i] != nullptr )
	{
	  delete m_entities[i];
	}
    }
  
}
