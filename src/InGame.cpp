#include <InGame.hpp>
#include <Core.hpp>
#include <Group.hpp>
#include <Entity.hpp>
#include <Rectangle.hpp>
#include <Block.hpp>
#include <ResourceLoader.hpp>

#include <random>
#include <chrono>
#include <cassert>

InGame::InGame(Core *owner): Scene(owner), m_rotation_delay(250), m_fall_delay(500), m_fall_delay_base(500), m_move_delay(100), m_current_block(nullptr), m_game_over(false), m_type_next_block(randBlockType()), m_next_block(nullptr), m_score(0)
{
  m_rotation_clock.restart();
  m_fall_clock.restart();
  m_move_clock.restart();
  
  m_interface = new Group;

  /* SIDE PANEL */
  m_interface->add(new Rectangle(BLOCK_WIDTH * NB_BLOCK_WIDTH,0,SIDE_WIDTH, SCREEN_HEIGHT,
				 sf::Color(25, 25, 50), 1));

  /* SCORE */
  m_score_font = m_owner->getResources()->getFont("madpixels");
  m_score_text.setFont(m_score_font);
  m_score_text.setPosition(sf::Vector2f(SIDE_X , BLOCK_HEIGHT/4 ));
  
  /* NEXT BLOCK */
  m_next_block = new Block( SIDE_X + BLOCK_WIDTH , BLOCK_HEIGHT*3 , m_type_next_block);
  defineNextBlock();
  m_interface->add( m_next_block );

  /* BACKGROUND */
  for(int i=0; i<NB_BLOCK_HEIGHT; i++)
    {
      for(int j=0; j<NB_BLOCK_WIDTH; j++)
	{
	  m_interface->add(new Rectangle(BLOCK_WIDTH*j ,BLOCK_HEIGHT*i,
	  				 BLOCK_WIDTH, BLOCK_HEIGHT, sf::Color(50,3*i+50,3*j+50), 1));

	  m_board[i][j] = nullptr;
	}
    }

  anotherBlock();

  m_owner->getResources()->playMusic("game");
}

bool InGame::isLineFull(size_t l)
{
  for(int j=0; j< NB_BLOCK_WIDTH; j++)
    {
      if( m_board[l][j] == nullptr )
	{
	  return false;
	}
    }
  
  return true;
}

void InGame::flushLine(size_t l)
{
  for(int j = 0; j < NB_BLOCK_WIDTH; j++)
    {
      if( m_board[l][j] != nullptr )
	{
	  delete m_board[l][j];
	  m_board[l][j] = nullptr;
	}
    }
      
    for(int i = l; i != 1 ; i--)
      {
	for(int j = 0; j < NB_BLOCK_WIDTH; j++)
	  {
	    m_board[i][j] = m_board[i-1][j];
	    
	    if( m_board[i][j] != nullptr )
	      {
		m_board[i][j]->move(0,BLOCK_HEIGHT);
	      }
	  }
      }
}

int InGame::randNumber(int a, int b)
{
  std::mt19937 re(std::chrono::steady_clock::now().time_since_epoch().count());
  std::uniform_int_distribution<int> dist(a, b);
  return dist(re);
}

BlockType InGame::randBlockType()
{
  switch(randNumber(0,6))
    {
    case 0: return BlockType::I;
    case 1: return BlockType::J;
    case 2: return BlockType::L;
    case 3: return BlockType::O;
    case 4: return BlockType::S;
    case 5: return BlockType::T;
    default: return BlockType::Z;
    }
}

void InGame::anotherBlock()
{
  assert(m_current_block == nullptr);

  for(int j=0; j<NB_BLOCK_WIDTH; j++)
    {
      if( m_board[0][j] != nullptr )
	{
	  delete m_current_block;
	  m_game_over = true;
	  return;
	}
    }

  //current
  m_current_block = new Block( ((NB_BLOCK_WIDTH-1)/2)*BLOCK_WIDTH , 0, m_type_next_block);

  //next
  defineNextBlock();
  
}

void InGame::defineNextBlock()
{
  m_type_next_block = randBlockType();
  
  m_next_block->reshape(m_type_next_block);
}

void InGame::updateKeyboard()
{
  //rotate
  if( m_current_block->isActive() &&  sf::Keyboard::isKeyPressed(sf::Keyboard::Space)
      && m_rotation_clock.getElapsedTime().asMilliseconds() > m_rotation_delay)
    {
      m_current_block->rotate();
      
      //if any collision after rotatation
      if( collideWithBoard(0, 0) )
	{
	  //undo the rotation by rotate three times
	  m_current_block->rotate();
	  m_current_block->rotate();
	  m_current_block->rotate();
	}
      else
	{
	  m_owner->getResources()->playSound("rotate");
	}
      
      m_rotation_clock.restart();

    }
    
  //move left
  int current_j = m_current_block->get(m_current_block->getXMin())->r->getPosition().x/BLOCK_WIDTH;

  if( m_current_block->isActive() &&  sf::Keyboard::isKeyPressed(sf::Keyboard::Left)
      && m_move_clock.getElapsedTime().asMilliseconds() > m_move_delay
      && current_j -1 >= 0
      && !collideWithBoard(0, -1) )
    {
      m_current_block->move(-BLOCK_WIDTH,0);
      m_move_clock.restart();
    }
      
  // move right
  current_j = m_current_block->get(m_current_block->getXMax())->r->getPosition().x/BLOCK_WIDTH;
  
  if( m_current_block->isActive() &&  sf::Keyboard::isKeyPressed(sf::Keyboard::Right)
      && m_move_clock.getElapsedTime().asMilliseconds() > m_move_delay
      && current_j + 1 < NB_BLOCK_WIDTH
      && !collideWithBoard(0, 1) )
    {
      m_current_block->move(BLOCK_WIDTH,0);
      m_move_clock.restart();
    }

  // move down
  if( m_current_block->isActive() &&  sf::Keyboard::isKeyPressed(sf::Keyboard::Down) )
    {
      m_fall_delay = 50;
    }
  else
    {
      m_fall_delay = m_fall_delay_base;
    }
}

void InGame::updateLines()
{
  for(int i=0; i<NB_BLOCK_HEIGHT; i++)
    {
      if( isLineFull(i) )
	{
	  m_score += SCORE_LINE_FULL;
	  m_owner->getResources()->playSound("line");
	  flushLine(i);
	}
    }
}

bool InGame::collideWithBoard(int I, int J)
{
  std::vector<Rectangle*> vec = m_current_block->explode();
	  
  for( Rectangle* r : vec )
    {
      int i = r->getPosition().y/BLOCK_HEIGHT;
      int j = r->getPosition().x/BLOCK_WIDTH;

      if( i + I >= NB_BLOCK_HEIGHT || j + J >= NB_BLOCK_WIDTH || i + I < 0 || j + J < 0)
	{
	  return true;
	}
      
      if( m_board[i+I][j+J] != nullptr )
	{
	  return true;
	}
    }
      
  return false;
}

void InGame::updateBlock()
{
  m_current_block->update();

  /* collide with board */
  if( collideWithBoard(1,0) )
    {
      m_current_block->desactive();
    }
      
  /* falling */
  if( m_current_block->isActive() && m_fall_clock.getElapsedTime().asMilliseconds() > m_fall_delay )
    {
      m_current_block->move(0,BLOCK_HEIGHT);
      m_owner->getResources()->playSound("fall");
      m_fall_clock.restart();
    }


  updateKeyboard();
      

  /* from block to board */
  if( !m_current_block->isActive() )
    {
      std::vector<Rectangle*> vec = m_current_block->explode();
	  
      for( Rectangle* r : vec )
	{
	  int i = r->getPosition().y/BLOCK_HEIGHT;
	  int j = r->getPosition().x/BLOCK_WIDTH;

	  m_board[i][j] = new Rectangle(r->getPosition().x, r->getPosition().y,
					BLOCK_WIDTH, BLOCK_HEIGHT, r->getColor(), r->zindex());
	}
	  
      delete m_current_block;
      m_current_block = nullptr;

      m_owner->getResources()->playSound("ground");
	    
      anotherBlock();
    }

}


void InGame::update()
{

  if( m_game_over )
    {
      m_owner->getResources()->stopMusic("game");
      m_owner->getResources()->playSound("end");
      m_owner->getResources()->waitSound("end");
      
      m_owner->gameOver(m_score);
      return;
    }
    
  /* INTERFACE */
  Scene::update();
  m_interface->update();

  m_score_text.setString(std::to_string(m_score) + " pts");
  
  /* CURRENT BLOCK */
  if( m_current_block != nullptr )
    {
      updateBlock();
    }

  /* BOARD */
  for(int i=0; i<NB_BLOCK_HEIGHT; i++)
    {
      for(int j=0; j<NB_BLOCK_WIDTH; j++)
	{
	  if( m_board[i][j]  != nullptr )
	    {
	      m_board[i][j]->update();
	    }
	}
    }

  updateLines();

}

void InGame::display(sf::RenderWindow &window)
{
  Scene::display(window);
  m_interface->display(window);

  window.draw(m_score_text);
  
  if( m_current_block != nullptr )
    {
      m_current_block->display(window);
    }

  /* board */
  for(int i=0; i<NB_BLOCK_HEIGHT; i++)
    {
      for(int j=0; j<NB_BLOCK_WIDTH; j++)
	{
	  if( m_board[i][j]  != nullptr )
	    {
	      m_board[i][j]->display(window);
	    }
	}
    }
  
}


void InGame::printBoard()
{
  std::cout<<"----------------------\n";//DEBUG
  for(int i = -1; i<NB_BLOCK_HEIGHT; i++)
    {
	  
      for(int j=-1; j<NB_BLOCK_WIDTH; j++)
	{
	  if( j == -1 && i != -1)
	    {
	      if( i <= 9) std::cout<<" "<<i<<" ";
	      else std::cout<<i<<" ";
	      continue;
	    }

	  if( j == -1 )
	    {
	      std::cout<<"   ";
	    }
	  else if( i == -1 )
	    {
	      if( j <= 9 )
		{
		  std::cout<<" "<<j<<" ";
		}
	      else
		{
		  std::cout<<" "<<j;
		}
	    }
	  else
	    {
	      int n = (m_board[i][j] != nullptr);
	      std::cout<<" "<<n<<" ";
	    }
	}std::cout<<std::endl;
      
    }
  std::cout<<"----------------------\n";
}

InGame::~InGame()
{
  if( m_interface != nullptr )
    {
      delete m_interface;
    }

    for(int i=0; i<NB_BLOCK_HEIGHT; i++)
    {
      for(int j=0; j<NB_BLOCK_WIDTH; j++)
	{
	  if( m_board[i][j]  != nullptr )
	    {
	      delete m_board[i][j];
	    }
	}
    }
}
