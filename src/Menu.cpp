#include <Menu.hpp>

#include <Core.hpp>
#include <ResourceLoader.hpp>

Menu::Menu(Core *owner): Scene(owner), m_menu_state(MENU_PLAY),
				 m_selected_color(sf::Color(200,50,200)),
				 m_unselected_color(sf::Color(50,50,50)),
				 m_navigate_delay(200)
{


  
  /* TITLE */
  m_font = m_owner->getResources()->getFont("madpixels");
    
  m_title.setFont(m_font);
  m_title.setCharacterSize(100);
  m_title.setColor(sf::Color(20, 100, 20));
  m_title.setString("YACOT");

  float x = (SCREEN_WIDTH - m_title.getLocalBounds().width)/2;
  float y = 2*SCREEN_HEIGHT/12 ;
  m_title.setPosition(sf::Vector2f(x, y));

  /* SUBTITLE */
  m_subtitle.setFont(m_font);
  m_subtitle.setCharacterSize(20);
  m_subtitle.setColor(sf::Color(200, 230, 200));
  m_subtitle.setString("Yet Another Clone Of Tetris");

  x = (SCREEN_WIDTH - m_subtitle.getLocalBounds().width)/2;
  y += m_title.getLocalBounds().height + m_subtitle.getLocalBounds().height;
  m_subtitle.setPosition(sf::Vector2f(x, y));

  /* SUBTITILE RECT */
  m_subtitle_rect.setSize(sf::Vector2f(SCREEN_WIDTH, 1.2 * m_subtitle.getLocalBounds().height));
  m_subtitle_rect.setPosition(sf::Vector2f(0, y));
  m_subtitle_rect.setFillColor(sf::Color(20, 100, 20));

  /* PLAY */
  m_play.setFont(m_font);
  m_play.setString("New game");
  m_play.setColor(sf::Color::Black);
  
  x = (SCREEN_WIDTH - m_play.getLocalBounds().width)/2;
  y = 6*SCREEN_HEIGHT/12;
  m_play.setPosition(sf::Vector2f(x, y));

  /* ABOUT */
  m_about.setFont(m_font);
  m_about.setString("About");
  m_about.setColor(sf::Color::Black);
  
  x = (SCREEN_WIDTH - m_about.getLocalBounds().width)/2;
  y = 7*SCREEN_HEIGHT/12;
  m_about.setPosition(sf::Vector2f(x, y));
  
  /* QUIT */
  m_quit.setFont(m_font);
  m_quit.setString("Quit");
  m_quit.setColor(sf::Color::Black);
  
  x = (SCREEN_WIDTH - m_quit.getLocalBounds().width)/2;
  y = 8*SCREEN_HEIGHT/12;
  m_quit.setPosition(sf::Vector2f(x, y));


  m_navigate_clock.restart();

  m_owner->getResources()->playMusic("menu");
}


void Menu::update()
{
  /* KEYBOARD */
  //up
  if(  sf::Keyboard::isKeyPressed(sf::Keyboard::Up)
      && m_navigate_clock.getElapsedTime().asMilliseconds() >= m_navigate_delay )
    {
      m_menu_state--;
      
      if( m_menu_state < MENU_PLAY )
	{
	  m_menu_state = MENU_QUIT;
	}
      
      m_owner->getResources()->playSound("navigate");
      m_navigate_clock.restart();
    }
       
  //down
  if(  sf::Keyboard::isKeyPressed(sf::Keyboard::Down)
      && m_navigate_clock.getElapsedTime().asMilliseconds() >= m_navigate_delay )
    {
      m_menu_state++;
      
      if( m_menu_state > MENU_QUIT )
	{
	  m_menu_state = MENU_PLAY;
	}
      
      m_owner->getResources()->playSound("navigate");
      m_navigate_clock.restart();
    }

  //ok
  if( sf::Keyboard::isKeyPressed(sf::Keyboard::Return)
      || sf::Keyboard::isKeyPressed(sf::Keyboard::Space) )
    {
      sf::Clock chrono;
      
      m_owner->getResources()->playSound("select");
      m_owner->getResources()->stopMusic("menu");
      
      //waiting for the sound
      while( chrono.getElapsedTime().asMilliseconds() < 1000 )
	{
	}
      
      switch( m_menu_state )
	{
	case MENU_PLAY:
	  m_owner->newGame();
	  break;

	case MENU_ABOUT:
	  m_owner->about();
	  break;

	case MENU_QUIT:
	  m_owner->quit();
	  break;
	}
    }

  /* COLORS */
  switch( m_menu_state )
    {
    case MENU_PLAY:
      m_play.setColor(m_selected_color);
      m_about.setColor(m_unselected_color);
      m_quit.setColor(m_unselected_color);
      break;

    case MENU_ABOUT:
      m_play.setColor(m_unselected_color);
      m_about.setColor(m_selected_color);
      m_quit.setColor(m_unselected_color);
      break;
      
    case MENU_QUIT:
      m_play.setColor(m_unselected_color);
      m_about.setColor(m_unselected_color);
      m_quit.setColor(m_selected_color);
      break;
    }
}

void Menu::display(sf::RenderWindow& window)
{
  window.clear(sf::Color(200, 200 , 230));
  window.draw(m_title);
  window.draw(m_subtitle_rect);
  window.draw(m_subtitle);
  window.draw(m_play);
  window.draw(m_about);
  window.draw(m_quit);
}

Menu::~Menu()
{
  
}
